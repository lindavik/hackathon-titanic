#!/usr/bin/env python

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="titanic",
    version="0.0.1",
    author="Linda Viksne",
    author_email="linda.viksne@gmail.com",
    description="Python hackathon project Titanic",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/lindavik/hackathon-titanic.git",
    packages=setuptools.find_packages(where='titanic_predictor'),
    package_dir={
        '': 'titanic_predictor',
    },
    classifiers=["Programming Language :: Python :: 3"],
    include_package_data = True,
    entry_points={
        'console_scripts': [
            'titanic = titanic.__main__:main'
        ]
    },
)
