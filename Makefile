inspect-code:
	pip install --upgrade pip
	pip install -r titanic_predictor/requirements-dev.txt
	pylint titanic_predictor
	black titanic_predictor/titanic
	mypy titanic_predictor --config-file config/mypy.ini

inspect-code-slim:
	black titanic_predictor/titanic
	mypy titanic_predictor --config-file config/mypy.ini
	pylint titanic_predictor

test:
	pytest

docs:
	make html

# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = source
BUILDDIR      = build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

