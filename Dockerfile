FROM python:3.7

COPY titanic_predictor/requirements.txt /
RUN python -m pip install --upgrade pip
RUN python -m pip download -d pip-dist -r requirements.txt
RUN python -m pip install --no-index --no-cache-dir --find-links ./pip-dist -r requirements.txt

WORKDIR /app

COPY ./titanic_predictor /app/titanic_predictor
EXPOSE 5000

ENTRYPOINT [ "bash", "run.sh" ]