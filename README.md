# Titanic Predictor

Python training challenge.

Install the package using the .tar.gz file here: https://gitlab.com/lindavik/hackathon-titanic/-/tree/master/dist

To use the library one of the commands below.

To train the machine learning model using a training data set "train.json": 
``titanic train -i train.json``

To evaluate the machine learning model using a validation data set "validate.json":
``titanic evaluate -i validate.json``

To predict the outcomes for a "predict.json" data set:  
``titanic predict -i predict.json``

To run the flask application:
``titanic serve ``
