pylint==2.5.2
black==19.10b0
mypy==0.770
make==0.1.6.post1
pre-commit==2.3.0
pytest==5.4.2
