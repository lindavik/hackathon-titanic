#!/usr/bin/env python3
""""Titanic application CLI module"""

import click
import pandas as pd
from titanic.app import app
from titanic.predservice import evaluate_model, predict_outcomes, MODEL_URI, train_model


@click.group()
def cli():  # pylint: disable=missing-function-docstring
    pass


@cli.command()
@click.option("-i", "--training-file", required=True)
def train(training_file):
    """Command for training model. Takes a json file containing the training data set"""
    click.echo("Training model")
    data = pd.read_json(training_file)
    train_model(data)
    click.echo(f"Finished training model. Model can be found in {MODEL_URI}")


@cli.command()
@click.option("-i", "--validation-file", required=True)
def evaluate(validation_file):
    """Command for evaluating model. Takes a json validation file"""
    click.echo("Evaluating model")
    data = pd.read_json(validation_file)
    evaluate_model(data)
    click.echo("Finished evaluating model")


@cli.command()
@click.option("-i", "--input-file", required=True)
@click.option("-o", "--output-file", required=True)
def predict(input_file, output_file):
    """Command for predicting outcomes.
    Takes a json input file for the predict data set
    and filename with json extension for output file"""
    click.echo("Predicting outcomes")
    data = pd.read_json(input_file)
    predictions = predict_outcomes(data)
    with open(output_file, "w") as output:
        output.write(predictions)
    click.echo("Done writing predictions")


@cli.command()
def serve():
    """Serves web app"""
    app.run()


if __name__ == "__main__":
    cli()
