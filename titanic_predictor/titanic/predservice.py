#!/usr/bin/env python3
"""Prediction service module for training and evaluating the model and predicting outcomes"""
from pathlib import Path

import pandas as pd
from pandas import DataFrame
from titanic.model import TitanicModel, Model

OUTPUT_URI = "outputs"
MODEL_URI = f"{OUTPUT_URI}/model.pkl"


def train_model(training_data: DataFrame):
    """"Trains the model using a given training data set"""
    model_path = Path(MODEL_URI)

    n_trees = 200
    train_data = training_data.drop("Survived", axis=1)
    survived_data = training_data["Survived"]

    model = TitanicModel(n_trees=n_trees)
    model.fit(train_data, survived_data)

    model_path.parent.mkdir(parents=True, exist_ok=True)
    model.save(model_path)


def evaluate_model(validation_data: DataFrame):
    """"Evaluates the created ML model against a validation data set"""
    model_path = Path(MODEL_URI)
    survived = validation_data.drop("Survived", axis=1)
    response_values = validation_data["Survived"]

    model = Model.load(model_path)
    metrics = model.evaluate(survived, response_values=response_values)

    for metric_name, metric_value in metrics.items():
        print(metric_name, metric_value, sep=": ")


def predict_outcomes(input_data: DataFrame):
    """Predicts outcomes for a given dataset """
    model_path = Path(MODEL_URI)
    model = Model.load(model_path)
    predictions = model.predict(input_data)
    predictions_df = pd.DataFrame({"prediction": predictions})
    return predictions_df.to_json()
