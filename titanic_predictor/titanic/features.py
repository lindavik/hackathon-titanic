"""Classes for selecting specific data from a given data set."""

from __future__ import annotations
import pandas as pd
from pandas import DataFrame
from sklearn.base import BaseEstimator, TransformerMixin


class ColumnSelector(BaseEstimator, TransformerMixin):
    """Selects columns from a pandas DataFrame."""

    def __init__(self, columns):
        self.columns = columns

    # fmt: off
    def fit(self, X, y=None) -> ColumnSelector:  # pylint: disable=unused-argument,invalid-name
        """Returns the specific ColumnSelector instance"""
        return self
    # fmt: on

    def transform(self, input_data: DataFrame) -> DataFrame:
        """Returns transformed input_data object with only specified columns"""
        if not isinstance(input_data, pd.DataFrame):
            raise ValueError(f"Expected Dataframe but got {type(input_data)}")

        try:
            return input_data[self.columns]
        except KeyError:
            missing_columns = list(set(self.columns) - set(input_data.columns))
            raise KeyError(
                f"The DataFrame does not include the columns: {missing_columns}"
            )
