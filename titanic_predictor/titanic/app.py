"""Module containing Flask app"""
import os
import random
import string

import pandas as pd
from flask import Flask, send_file, after_this_request
from flask import request
from titanic.predservice import predict_outcomes

app = Flask(__name__)

UPLOAD_FOLDER = "titanic_predictor/data"
ALLOWED_EXTENSIONS = {"json"}

app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER


@app.route("/")
def home():
    """Home endpoint"""
    return "Titanic app"


@app.route("/ping")
def ping():
    """Ping pong endpoint"""
    return "pong", 200


def allowed_file(filename):
    """Checks if the file extension is allowed"""
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/predict", methods=["POST"])
def predict():  # pylint: disable=inconsistent-return-statements
    """Endpoint for predicting outcomes using trained ML model"""
    if "file" not in request.files or request.files["file"] == "":
        return "No file provided", 400
    file = request.files["file"]
    if not allowed_file(file.filename):
        return "Unsupported file type", 400
    if file:
        data = pd.read_json(file.read())
        print("Finished reading file")
        predictions = predict_outcomes(input_data=data)
        tail = generate_random_tail()
        file_uri = f"outputs/predictions_{tail}.json"
        with open(file_uri, "w") as outfile:
            outfile.write(predictions)
        print("Finished writing file")

        @after_this_request
        def remove_file(response):  # pylint: disable=unused-variable
            os.remove(file_uri)
            print(f"Removed {file_uri}")
            return response

        return send_file(file_uri, as_attachment=True)


def generate_random_tail():
    """Generates random tail"""
    return "".join(random.choices(string.ascii_letters + string.digits, k=8))


if __name__ == "__main__":
    app.run()
