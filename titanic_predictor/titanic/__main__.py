"""Module for running CLI app"""
import sys

from .cli import cli


def main(args=None):  # pylint: disable=missing-function-docstring
    if args is None:
        args = sys.argv[1:]
    cli()


if __name__ == "__main__":
    main()
