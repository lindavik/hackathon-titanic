import pytest
from pandas import DataFrame
import pandas as pd

from titanic_predictor.titanic.model import Model, TitanicModel, NotFitError

SAMPLE_PREDICT_DATA_FILE = "titanic_predictor/tests/titanic/test_data/predict.csv"
SAMPLE_TRAIN_DATA_FILE = "titanic_predictor/tests/titanic/test_data/train.csv"
DEFAULT_N_TREE = 200


def test_get_params():
    model: Model = TitanicModel()
    expected: dict = {"n_trees": DEFAULT_N_TREE}

    assert model.get_params() == expected


def test_predict_before_fit():
    with pytest.raises(NotFitError):
        model: Model = TitanicModel()
        input_data: DataFrame = get_input_data_as_dataframe()

        model.predict(input_data)


def get_input_data_as_dataframe() -> DataFrame:
    return pd.read_csv(SAMPLE_PREDICT_DATA_FILE)
