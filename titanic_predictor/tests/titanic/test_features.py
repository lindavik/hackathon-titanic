import csv
import pandas as pd
import pytest
from pandas import DataFrame

from titanic_predictor.titanic.features import ColumnSelector

SAMPLE_DATA_FILE = "titanic_predictor/tests/titanic/test_data/predict.csv"


def test_fit():
    column_selector: ColumnSelector = ColumnSelector(columns=["Pclass", "Sex"])

    assert column_selector.fit(None).columns == ["Pclass", "Sex"]


def test_fit_no_columns_specified():
    column_selector: ColumnSelector = ColumnSelector(columns=None)

    assert column_selector.fit(None).columns == None


def test_transform_wrong_input_data_type():
    with pytest.raises(ValueError):
        input_data = get_input_data_as_dict()
        column_selector: ColumnSelector = ColumnSelector(columns=["Pclass", "Sex"])

        column_selector.transform(input_data)


def test_transform_missing_column():
    with pytest.raises(KeyError):
        input_data = get_input_data_as_dataframe()
        column_selector: ColumnSelector = ColumnSelector(
            columns=["Pclass", "Favourite food"]
        )

        column_selector.transform(input_data)


def test_transform_happy_path():
    input_data = get_input_data_as_dataframe()
    columns = ["Pclass", "Sex"]
    column_selector: ColumnSelector = ColumnSelector(columns=columns)

    result: DataFrame = column_selector.transform(input_data)
    print(result)

    assert len(result.columns) == 2
    assert result.columns.tolist() == columns


def get_input_data_as_dict() -> dict:
    with open(SAMPLE_DATA_FILE, mode="r") as infile:
        reader = csv.reader(infile)
        sample_data = {rows[0]: rows[1] for rows in reader}
        return sample_data


def get_input_data_as_dataframe() -> DataFrame:
    return pd.read_csv(SAMPLE_DATA_FILE)
