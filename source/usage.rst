Usage
===================================

To try the application, download the sample data available in titanic_predictor/data:

* to train the model use train.csv

* to validate the model use validation.csv

* to make predictions use predict.csv

For sample uses check the titanic_predictor/data folder, for best viewing open it using Jupyter Notebooks.
For an example on how to fit the model, check the 01_train notebook.