#################
About
#################

Titanic is an application that uses machine learning to predict the survival of Titanic passengers.
The application is written in Python and is part of a Python training project.


.. toctree::
     :maxdepth: 2

     installation
     usage


Documentation
=================

Features
-----------------
.. automodule:: titanic.features
    :members:

Model
-----------------
.. automodule:: titanic.model
    :members:






Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
