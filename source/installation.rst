Installation
===================================

To install the package:

* download the titanic .tar.gz file in https://gitlab.com/lindavik/hackathon-titanic/-/tree/master/dist

* "pip install" the file

